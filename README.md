# IsardVDI Selenium tests

- Install Selenium IDE in your firefox browser
- Open and record actions. Reply/edit till it works.
- Do it with web English web interface always
- Test should leave always system as it was
- Remove url and urls keys from .side selenium file (will be taken from config.yaml)
- Save it to sides folder
- Test it with run multiple times to see that it is reproducible

# Run

Copy config.yaml.example to config.yaml and set baseURL parameter to your clean IsardVDI install.
```
docker-compose up -d 
```

NOTE: Be aware that isard-tests container needs to wait for firefox container to be fully up (maybe a healthcheck/depends could be done at port 4444).

Output will be found in out folder.
